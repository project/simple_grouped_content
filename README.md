#Simple Grouped Content

Simple grouped content seeks to take Group and a few other contrib modules and
provide a simplified install profile for managing grouped content.

##Structural Concept.

Group suffers from the typical Drupal problem where it is nearly unlimitedly
flexible and capable. In doing so however, it makes setting something simple up a bit arduous.
Beyond that modules like "group_content_menu" also seek to provide infinte
flexibility and you can associate unlimited groups with a menu.

**Simple Grouped Content** works by having a 1:1 relationship between content 
(nodes, menus) and groups. From there, **group_content** is king and what
visitors and users will interact with the most. Each group also gets a single
menu, and there is a tab to edit the group in the menu. When wishing to place
group content in the group menu, simply do so by editing the group_content
entity itself.

Metatags are also placed on the group content, and group_content will
display the default display mode of a given node.

A tab to edit the content (node) has been added for group content, helping users
easily navigate to editing the node. Nodes themselves that are associated with a
group however can not be viewed directly, viewers will be forwarded to the
corresponding piece of group content.

It may sound complex, but I assure you... this is the simplest implementation
of the Group module you will find.
