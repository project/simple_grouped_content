<?php

namespace Drupal\sgc_support_module\EventSubscriber;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\value_fetcher\ValueFetcher;

/**
 * Subscribe to KernelEvents::REQUEST events.
 *
 * Redirect node view requests if there is a related group
 * content entity available.
 *
 * Redirect group content views to group if referenced as primary piece
 * of content.
 */
class GroupContentRedirectSubscriber implements EventSubscriberInterface {

  /**
   * The request event.
   *
   * @var \Symfony\Component\HttpKernel\Event\RequestEvent
   */
  private $event;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['sgcSupportModuleGroupsRedirection'];
    return $events;
  }

  /**
   * Catch node view loads and redirect if single related group content.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event object as passed from the event.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function sgcSupportModuleGroupsRedirection(RequestEvent $event)
  : void {
    $this->event = $event;
    $request = $this->event->getRequest();

    // If accessing gcontent.
    if ($request->attributes->get('group_content')) {
      $group = $request->attributes->get('group');
      $group_content = $request->attributes->get('group_content');

      // Check if gcontent primary content for group, if so redirect to group.
      if (
        ValueFetcher::getFirstValue($group, 'field_group_homepage')
        === $group_content->getEntity()->id()
      ) {
        $this->doRedirect($group->toUrl());
        return;
      }
    }

    // If attempting to directly access a node, redirect to group_content.
    if (
      in_array($request->attributes->get('_route'), [
        'entity.node.latest_version',
        'entity.node.canonical',
      ])
    ) {
      /** @var \Drupal\Core\Entity\EntityInterface $group_content */
      $group_content = sgc_support_module_load_group_content_from_node_values(
        $request->attributes->get('node')->id(),
        $request->attributes->get('node')->bundle()
      );
      if ($group_content instanceof EntityInterface) {
        $this->doRedirect($group_content->toUrl());
      }
    }
    elseif (
      // Check group content that is referenced by group field, redirect if so.
      $request->attributes->has('group_content')
      && $request->attributes->has('group')
    ) {
      /** @var \Drupal\group\Entity\Group $group */
      $group = $request->get('group');
      if (!isset($group) && $group->hasField('field_group_homepage')) {
        $entity = ValueFetcher::getAllValues($group, 'field_group_homepage');
        /** @var \Drupal\group\Entity\GroupContent $group_content */
        $group_content = $request->get('group_content');
        if (
          isset($entity['target_id'])
          && $entity['target_id'] === $group_content->getEntity()->id()
        ) {
          $this->doRedirect($group->toUrl());
        }
      }
    }
  }

  /**
   * Sets the redirect response.
   *
   * @param \Drupal\Core\Url $url
   *   The destination URL for the redirect.
   */
  private function doRedirect(Url $url)
  : void {
    $response = new RedirectResponse($url->toString(), 301);
    $this->event->setResponse($response);
  }

}
