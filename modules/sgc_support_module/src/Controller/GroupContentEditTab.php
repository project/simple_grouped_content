<?php

namespace Drupal\sgc_support_module\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\GroupContentInterface;
use Drupal\group\Entity\GroupInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\value_fetcher\ValueFetcher;

/**
 * Provides group content edit functionality.
 */
class GroupContentEditTab extends ControllerBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Constructs a new GroupMembershipController.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Logger for logging errors or warnings.
   */
  public function __construct(AccountInterface $current_user, LoggerChannelFactoryInterface $logger) {
    $this->currentUser = $current_user;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  : GroupContentEditTab {
    return new static(
      $container->get('current_user'),
      $container->get('logger.factory')
    );
  }

  /**
   * Provides the form for joining a group.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group to join.
   * @param \Drupal\group\Entity\GroupContentInterface $group_content
   *   The relevant group content entity.
   *
   * @return mixed
   *   A redirect or a blank render array.
   */
  public function editNodeForward(GroupInterface $group, GroupContentInterface $group_content) {
    if ($group_content->getContentPlugin()->getEntityTypeId() === 'node') {
      $nid = ValueFetcher::getAllValues($group_content, 'entity_id')['target_id'];
      $destination = '/group/' . $group->id() . '/content/' . $group_content->id();

      return $this->redirect(
        'entity.node.edit_form',
        ['node' => $nid],
        ['query' => ['destination' => $destination]]
      );
    }

    // In theory we never get here, access checks route for gcontent being node.
    return [
      '#markup' => '',
    ];
  }

  /**
   * Custom access check for the Content Edit link.
   *
   * @param \Drupal\group\Entity\GroupContentInterface $group_content
   *   The group content to check for access.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function accessTab(GroupContentInterface $group_content)
  : AccessResult {
    $access_result = AccessResult::forbidden();

    if ($group_content->getContentPlugin()->getEntityTypeId() === 'node') {
      if ($group_content->access('update', $this->currentUser)) {
        $access_result = AccessResult::allowed();
      }
    }
    else {
      $message = $this->t('Group content of type other than node passed to editNodeForward. Group content ID: @GCID', [
        '@GCID' => $group_content->id(),
      ]);
      $this->logger->get('sgc_support_module')->error($message);
    }

    return $access_result;
  }

}
