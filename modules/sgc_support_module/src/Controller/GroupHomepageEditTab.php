<?php

namespace Drupal\sgc_support_module\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\group\Entity\GroupInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\Entity\Node;
use Drupal\value_fetcher\ValueFetcher;

/**
 * Provides group content edit functionality.
 */
class GroupHomepageEditTab extends ControllerBase {

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Constructs a new GroupMembershipController.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Logger for logging errors or warnings.
   */
  public function __construct(LoggerChannelFactoryInterface $logger) {
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory')
    );
  }

  /**
   * Access check on the group homepage node.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The unique profile ID.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function access(GroupInterface $group)
  : AccessResultInterface {
    $allowed = FALSE;

    $node = self::loadGroupHomepage($group);

    if ($node) {
      if ($node->access('update')) {
        $allowed = TRUE;
      }
    }

    // Determine if the user is allowed access.
    if ($allowed) {
      return AccessResult::allowed()
        ->addCacheableDependency($node)
        ->addCacheableDependency($group)
        ->cachePerUser();
    }
    else {
      return AccessResult::forbidden()
        ->addCacheableDependency($node)
        ->addCacheableDependency($group)
        ->cachePerUser();
    }
  }

  /**
   * Provides the form for joining a group.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group to join.
   *
   * @return mixed
   *   A redirect or a blank render array.
   */
  public function editHomepageForward(GroupInterface $group) {
    $nid = ValueFetcher::getFirstValue($group, 'field_group_homepage');

    // If the group has a node, redirect them to it's edit form.
    if ($nid) {
      $destination = '/group/' . $group->id();
      return $this->redirect(
        'entity.node.edit_form',
        ['node' => $nid],
        ['destination' => $destination]
      );
    }
    else {
      $message = $this->t('User attempted to edit group homepage for group @GID which does not have a homepage.', [
        '@GID' => $group->id(),
      ]);
      $this->logger->get('sgc_support_module')->error($message);

      return [
        '#markup' => '',
      ];
    }
  }

  /**
   * Abstract the loading of a group's homepage node.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   A fully qualified group in which to look for rendered content / homepage.
   *
   * @return \Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface|false
   *   Loaded node for the group home page, or FALSE if there is none.
   */
  private static function loadGroupHomepage(GroupInterface $group) {
    $nid = ValueFetcher::getFirstValue($group, 'field_group_homepage');

    if ($nid) {
      return node::load($nid);
    }

    return FALSE;
  }

}
